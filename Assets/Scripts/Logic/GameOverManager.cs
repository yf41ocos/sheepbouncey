﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {

    public bool gameover;       // Reference to the player's health.
    public float restartDelay = 5f;         // Time to wait before restarting the level


    Animator anim;                          // Reference to the animator component.
    public float restartTimer;                     // Timer to count up to restarting the level


    void Awake()
    {
        // Set up the reference.
        restartTimer = 0;
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        // If the player has run out of health...
        if (gameover)
        {

			SceneManager.LoadScene("gameover");
            // ... tell the animator the game is over.
           // anim.SetTrigger("GameOver");
            //Application.LoadLevel(Application.loadedLevel);
            // .. increment a timer to count up to restarting.
           /* restartTimer += Time.deltaTime;

            // .. if it reaches the restart delay...
            if (restartTimer >= restartDelay)
            {
                // .. then reload the currently loaded level.
               // Application.LoadLevel(Application.loadedLevel);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }*/
        }
    }
}
