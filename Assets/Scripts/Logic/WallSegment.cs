using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace WallAssembly
{
	public class Pair<T, U> {
		public Pair() {
		}
		
		public Pair(T first, U second) {
			this.First = first;
			this.Second = second;
		}
		
		public T First { get; set; }
		public U Second { get; set; }
		
		public bool Equals(Pair<T,U> other)
		{
			return ((First.Equals(other.First))&&(Second.Equals(other.Second)));
		}
	};
	
	public enum Direction{HOR,VERT,BACK,FOR};
	
	public class WallSegment : MonoBehaviour
	{
		private metagrid grid;
		private int num_bricks_width, num_bricks_height;
		private float scale_x, scale_z;
		private Vector2 position;
		private Direction horvert;
		private Direction dir;
		
		
		private int x_id, y_id;
		private GameObject last;
        private List<GameObject> objects;
        private int cnt;

		public int start_x{ get; internal set; }
        public int start_y{ get; internal set; }
        public int end_x{ get; internal set; }
        public int end_y{ get; internal set; }

        private Bounds b_plank, b_pole;
        private float pole_scale_a, pole_scale_b;
        private float height;

        private bool preview;
        void Start(){

		}

		public void init(metagrid g, int xid, int yid ,  Direction horvert_, Direction backfor,
							int nw, int nh, int c, float height, bool is_preview)
		{
            objects = new List<GameObject>();
            this.height = height;
            preview = is_preview;
            cnt = c;
			x_id = xid; y_id = yid;

			start_x = x_id;
			start_y = y_id;
			
			end_x = start_x;
			end_y = start_y;

			grid = g;
			num_bricks_height = nh;
			num_bricks_width = nw;
			horvert = horvert_;
			dir = backfor;
			position = grid.getCellPos(x_id,y_id);
			
			b_plank = grid.plank_double.transform.GetComponent<MeshFilter>().sharedMesh.bounds;
            b_pole = grid.pole.transform.GetComponent<MeshFilter>().sharedMesh.bounds;

            if (horvert == Direction.VERT)
            {
                pole_scale_a = grid.h_width / b_pole.size.x;
                pole_scale_b = grid.h_height / b_pole.size.z;
            }
            else
            {
                pole_scale_a = grid.h_height / b_pole.size.z;
                pole_scale_b = grid.h_width / b_pole.size.x;
            }

            if (horvert == Direction.VERT) scale_z = (grid.h_width / (b_plank.size.z * 3.0F));
            else scale_z = (grid.h_height / (b_plank.size.z * 3.0F));

        }


        private float max(Transform t, short m)
        {
            Vector3[] vs = t.GetComponent<MeshFilter>().sharedMesh.vertices;
            float biggest = t.TransformPoint(vs[0])[m];
            foreach (Vector3 v_local in vs)
            {
                Vector3 v_world = t.TransformPoint(v_local);
                if (v_world[m] > biggest)
                {
                    biggest = v_world[m];
                }
            }
            return biggest;
        }

        private float min(Transform t, short m)
        {
            Vector3[] vs = t.GetComponent<MeshFilter>().sharedMesh.vertices;
            float smallest = t.TransformPoint(vs[0])[m];
            foreach (Vector3 v_local in vs)
            {
                Vector3 v_world = t.TransformPoint(v_local);
                if (v_world[m] < smallest)
                {
                    smallest = v_world[m];
                }
            }
            return smallest;
        }

        public Vector3 allign (GameObject left, GameObject right)
        {
            float translation = 0.0F;
            if (horvert == Direction.HOR)
            {
                if (dir == Direction.FOR) translation = max(left.transform, 0) - min(right.transform, 0);
                else if (dir == Direction.BACK) translation = min(left.transform, 0) - max(right.transform, 0);
                return new Vector3(translation,0,0);
            }
            else
            {
                if (dir == Direction.FOR) translation = max(left.transform, 2) - min(right.transform, 2);
                else if (dir == Direction.BACK) translation = min(left.transform, 2) - max(right.transform, 2);
                return new Vector3(0, 0, translation);
            }
        }


        public GameObject setPole(Vector3 pos)
        {
            int x, y;
            grid.getCellCoords(new Vector2(pos.x,pos.y),out x, out y);
            if(!preview) grid.getCell(y, x).contains_wall = true;

            GameObject pole = (GameObject)Instantiate(grid.pole, new Vector3(pos.x, 0.0F, pos.y),
                        Quaternion.Euler(new Vector3(0.0F, ((horvert == Direction.HOR) ? 90.0F : 0.0F), 0)));

            pole.transform.localScale = new Vector3(pole_scale_a, height, pole_scale_b);

            pole.setParent(this.gameObject, true);
            return pole;
        }

        public GameObject setPole(Vector3 pos, GameObject prev)
        {
            GameObject pole = setPole(pos);
            Vector3 translation = allign(prev, pole);
            pole.transform.Translate(translation, Space.World);
            return pole;
        }

        public GameObject setPlank(Vector3 pos, int length, GameObject prev)
        {
            float factor_z, factor_x;

            if (horvert == Direction.VERT)
            {
                factor_x = ((float)length * grid.h_height) / b_plank.size.x;
            }
            else
            {
                factor_x = ((float)length * grid.h_width) / b_plank.size.x;
            }
            factor_z = scale_z;
            GameObject o = (GameObject)Instantiate(grid.plank_double, new Vector3(pos.x, 0.0F, pos.y),
                Quaternion.Euler(new Vector3(0.0F, ((horvert == Direction.VERT) ? 90.0F : 0.0F), 0)));
            o.transform.localScale = new Vector3(factor_x, height, factor_z);

            Vector3 translation = allign(prev,o);
            o.transform.Translate(translation, Space.World);


            //mark cells
            float first, second; short selection = (short)((horvert == Direction.HOR) ? 0 : 2);
           // Debug.Log("selection = "+selection);
            first = min(o.transform, selection);
            second = max(o.transform, selection);

            if (horvert == Direction.HOR)
            {
                //x value is important - first is min x second is max x
                int min_x, max_x , y;
                grid.getCellCoords(new Vector2(first, pos.y), out min_x, out y);
                grid.getCellCoords(new Vector2(second, pos.y), out max_x, out y);

                for (int i = min_x; i <= max_x; ++i )
                {
                    if(!preview) grid.getCell(y, i).contains_wall = true;
					if(Direction.FOR == dir){end_y = y; end_x = i;}
					else { end_y = y; end_x = min_x; }
                }
            }
            else
            {
                //x value is important - first is min x second is max x
                int min_y, max_y, x;
                grid.getCellCoords(new Vector2(pos.x,first), out x, out min_y);
                grid.getCellCoords(new Vector2(pos.x,second), out x, out max_y);

                for (int i = min_y; i <= max_y; ++i)
                {
                    if (!preview) grid.getCell(i, x).contains_wall = true;

					if(Direction.FOR == dir){end_y = i; end_x = x;}
					else { end_y = min_y; end_x = x; }

                }
            }


            o.setParent(this.gameObject, true);
            
            return o;
        }

        private void setPreview(GameObject obj)
        {
            obj.GetComponentInChildren<Renderer>().material.color = new Vector4(1, 0, 0, 0.1f);
            obj.transform.parent.tag = "preview";
            obj.GetComponent<Collider>().enabled = false;
        }

        private void InitializeIfNeeded(ParticleSystem sys,out ParticleSystem.Particle[] particles)
        {       
                particles = new ParticleSystem.Particle[sys.maxParticles];
        }

        public void setWall(){

            if (cnt > 0)
            {
                GameObject pole = setPole(position);
                if (preview)
                {
                    setPreview(pole);
                    objects.Add(pole);
                } else pole.transform.parent.tag = "is_building";
                int comp = (horvert == Direction.HOR) ? num_bricks_width : num_bricks_height;

                if (!preview && false)
                {
                    var go = new GameObject("particles");
                    go.AddComponent<ParticleSystem>();
                    go.transform.position = pole.transform.position;
                    ParticleSystem sys = go.GetComponent<ParticleSystem>();

                    ParticleSystem.Particle[] particles;
                    InitializeIfNeeded(sys,out particles);

                    int numParticles = 1000;
                    go.GetComponent<ParticleSystem>().GetParticles(particles);

                    for (int i = 0; i < numParticles; ++i)
                    {
                        particles[i].startColor = new Color32(255, 0, 0, 255);
                        particles[i].position = pole.transform.position;
                        particles[i].lifetime = 0.0f;
                        particles[i].startLifetime = 1.0f;
                        Color32 col = particles[i].GetCurrentColor(go.GetComponent<ParticleSystem>());
                        Debug.Log(col.r+" "+ col.g+" " +col.b);
                        particles[i].velocity = new Vector3(0, 2, 0);
                    }

                    go.GetComponent<ParticleSystem>().SetParticles(particles, numParticles);
                }
                

                    if (cnt <= comp)
                {
                    GameObject plank = setPlank(position, cnt, pole);
                    if (preview)
                    {
                        setPreview(plank);
                        objects.Add(plank);
                    } else plank.transform.parent.tag = "is_building";

                    pole = setPole(position,plank);
                    if (preview)
                    {
                        setPreview(pole);
                        objects.Add(pole);
                    } else pole.transform.parent.tag = "is_building";
                }
                else
                {
                    //build normal wall
                    GameObject plank = setPlank(position, (comp - 1), pole);
                    if (preview)
                    {
                        setPreview(plank);
                        objects.Add(plank);
                    } else plank.transform.parent.tag = "is_building";
                }
                last = pole;
            }

		}
		
		public bool advance(){

			if(cnt > 0){

				if(horvert == Direction.VERT){

					cnt -= num_bricks_height;
					y_id = (dir == Direction.FOR) ? (y_id+num_bricks_height) : (y_id-num_bricks_height);
	                
				} else {

					cnt -= num_bricks_width;
					x_id = (dir == Direction.FOR) ? (x_id+num_bricks_width) : (x_id-num_bricks_width);

	            }
			}
			position = grid.getCellPos(x_id,y_id);

            return cnt <= 0;
		}

        public void setPreview()
        {
            if (preview)
            {
                bool done = false;
                while (!done)
                {
                    setWall();
                    done = advance();
                }
            }
        }

        public void disablePreview()
        {
            if (preview)
            {
                foreach(GameObject obj in objects)
                {
                    obj.SetActive(false);
                }
            }
        }

        public void enablePreview()
        {
            if (preview)
            {
                foreach (GameObject obj in objects)
                {
                    obj.SetActive(true);
                }
            }
        }

        public void destroyPreview()
        {
            if (preview)
            {
                foreach (GameObject obj in objects)
                {
                   Destroy(obj);
                }
                objects.Clear();
            }
        }

        public void deactivate_area(Vector3 sheep_pos)
		{
            last.transform.parent.gameObject.tag = "is_done";

            int sx, sy;
			grid.getCellCoords(new Vector2(sheep_pos.x,sheep_pos.z),out sx, out sy);

			//left == 0, right == 1
			short sheep_left_right;
			if (horvert == Direction.VERT){
				sheep_left_right = (short)((sx < start_x) ? 0 : 1);
			} else {
				sheep_left_right = (short)((sy < start_y) ? 0 : 1);
			}


			if(horvert == Direction.VERT){
				int i=0,i_end=0;
				if(Direction.FOR == dir){
					i = start_y; i_end = end_y;
				} else {
					i = end_y; i_end = start_y;
				}

				if (sheep_left_right == 1){
					//left
					for (; i <= i_end; ++i){
						for (int j = start_x-((num_bricks_width-1)/2 + 1);j >=0;--j){
							if(grid.getCell(i, j).contains_wall) break;
							grid.getCell(i, j).deactivated = true;
						}
					}
				} else {
					//right
					for (; i <= i_end; ++i){
						for (int j = start_x+((num_bricks_width-1)/2 + 1);j < grid.grid_width;++j){
							if(grid.getCell(i, j).contains_wall) break;
							grid.getCell(i, j).deactivated = true;
						}
					}
				}
			} else { //Horizontal

				int i=0,i_end=0;
				if(Direction.FOR == dir){
					i = start_x; i_end = end_x;
				} else {
					i = end_x; i_end = start_x;
				}

				if (sheep_left_right == 1){
					//down
					for (; i <= i_end; ++i){
						for (int j = start_y-((num_bricks_height-1)/2 + 1);j >=0;--j){
							if(grid.getCell(j, i).contains_wall) break;
							grid.getCell(j, i).deactivated = true;
						}
					}
				} else {
					//up
					for (; i <= i_end; ++i){
						for (int j = start_y+((num_bricks_height-1)/2 + 1);j < grid.grid_height;++j){
							if(grid.getCell(j, i).contains_wall) break;
							grid.getCell(j, i).deactivated = true;
						}
					}
				}
			}
		}

	}
}

