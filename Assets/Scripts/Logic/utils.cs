using System;
using UnityEngine;
using sd=System.Diagnostics;

public static class utils
{

	public static void setParent(this GameObject child, GameObject parent)
	{
		sd.Debug.Assert(child != null);
		sd.Debug.Assert(parent != null);
		sd.Debug.Assert(child.transform != null);
		sd.Debug.Assert(parent.transform != null);
		child.transform.SetParent(parent.transform);
	}
	
	public static void setParent(this GameObject child, GameObject parent, bool worldPositionStays)
	{
		child.transform.SetParent(parent.transform, worldPositionStays);
	}

}


