﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WallAssembly;
using UnityEngine.SceneManagement;

public class metagrid : MonoBehaviour {

	private Pair<int, int> numblocks_leftright;
	private Pair<int, int> numblocks_updown;

	public bool debug_draw;
	public bool draw_grid;
	private int numFilledCells;
	public Camera cam;

	public float width;
	public float height;

	public int grid_width;
	public int grid_height;

	public float h_width;
	public float h_height;

	public GameObject brick;
	public GameObject fence;
	public GameObject plank_double;
	public GameObject pole;

	public  float uz, dz, lx, rx;

	public float wall_width, wall_height;
	
	public float time_to_grow;
    public float height_of_fence_real_world;

    public levelselector levelsel;

	public List<simple_sphere> sheeps;
	public RULE.GameRule gamerule;

	public int enlistSheep(simple_sphere sheep)
	{
		sheeps.Add(sheep);
		return sheeps.Count;
	}

    public Vector2 getCellPos(int x, int y){
		return new Vector2((x*h_width + h_width/2.0F +lx),(y*h_height + h_height/2.0F + dz));
	}
	
	public class Cell {
		public int x,y;
		public Vector2 pos;
		public bool contains_sheep = false;
		public bool contains_wall = false;
		public bool deactivated = false;
        public bool clicked = false;
		public GameObject ownbrick;

		private metagrid g_;
	
		public Cell(metagrid g, int x_ , int y_){
			g_ = g;
			if(g.debug_draw)ownbrick = new GameObject();
			x = x_; y = y_;
			pos = g.getCellPos(x,y);
			contains_wall = false;
			contains_sheep = false;
			deactivated = false;
            clicked = false;
        }


		public void draw(Vector3 offset = default(Vector3), Color col = default(Color)){
		
			Color c;
			if(contains_wall){
				c = Color.black;
				Debug.DrawLine (new Vector3 (pos.x - g_.h_width/2.0F, 2.0F, pos.y - g_.h_height/2.0F) + offset, 
                    new Vector3 (pos.x + g_.h_width/2.0F, 2.0F, pos.y + g_.h_height/2.0F) + offset, c);
			} else if (deactivated){
				c = Color.blue;
			} else c = Color.red;

            c += col;

			Debug.DrawLine (new Vector3 (pos.x - g_.h_width/2.0F, 2.0F, pos.y - g_.h_height/2.0F) + offset, 
                new Vector3 (pos.x + g_.h_width/2.0F, 2.0F, pos.y - g_.h_height/2.0F) + offset, c);
			Debug.DrawLine (new Vector3 (pos.x + g_.h_width/2.0F, 2.0F, pos.y - g_.h_height/2.0F) + offset, 
                new Vector3 (pos.x + g_.h_width/2.0F, 2.0F, pos.y + g_.h_height/2.0F) + offset, c);
		}

		public void click(Color c){
			ownbrick = (GameObject)Instantiate(g_.brick, new Vector3(pos.x,0.5F,pos.y), Quaternion.identity);
			ownbrick.transform.localScale = new Vector3(g_.h_width,5.5F,g_.h_height);
			ownbrick.GetComponent<Renderer>().material.color = c;
		}
	}

	private Cell[,] grid;

	public Cell getCell(int x, int y){return grid[x,y];}

	public void getCellCoords(Vector2 pos, out int x, out int y){
		float xf = pos.x-lx;
		float yf = pos.y-dz;
		
		x = Mathf.FloorToInt((float)(xf/h_width));
		y = Mathf.FloorToInt((float)(yf/h_height));
		
		if(x < 0) x=0; if(y < 0) y=0;
		if(x >= grid_width) x=grid_width-1; if(y >= grid_height) y=grid_height-1;
		
	}

	Mesh getMesh(string name){
		GameObject go = GameObject.Find(name);
		MeshFilter viewedModelFilter = (MeshFilter)(go.GetComponent("MeshFilter"));
		return viewedModelFilter.mesh; 
	}

	Transform getTransform(string name){
		GameObject go = GameObject.Find(name);
		return go.transform;
	}

	Vector3[] trverts (string name){
		Mesh mesh = getMesh(name);
		Vector3[] inputverts = mesh.vertices;
		Transform transform = getTransform(name);
		Vector3[] output = new Vector3[inputverts.GetLength(0)];
		for (int i = 0; i < inputverts.GetLength(0); ++i)
		{
			output[i] = transform.TransformPoint(inputverts[i]);
		}
		return output;
	}

	enum Mode {MIN, MAX};
	float minmaxVec (Vector3[] v,int pos, Mode m) {
		float min_max;
		if(m == Mode.MIN){
			min_max = float.MaxValue;
			for (int i = 0; i < v.GetLength(0); ++i)
			{
				if (v[i][pos] < min_max) {
					min_max = v[i][pos];
				}
			}
		} else {
			min_max = float.MinValue;
			for (int i = 0; i < v.GetLength(0); ++i)
			{
				if (v[i][pos] > min_max){
					min_max = v[i][pos];
				} 
			}
		}
		return min_max;
	}

	//Pair id x/y
	public void BlockRegion (Pair<int,int> id, out Pair<int,int> left_down, out Pair<int,int> right_up, short dir) {
		int x_id = id.First;
		int y_id = id.Second;
		int num_bricks_width = 0;
		int num_bricks_height = 0;
		if(dir == 1){
			num_bricks_width = numblocks_updown.Second;
			num_bricks_height = numblocks_updown.First;
		} else {
			num_bricks_width = numblocks_leftright.Second;
			num_bricks_height = numblocks_leftright.First;
		}
		int y_down = (y_id - num_bricks_height/2); y_down = (y_down < 0) ? 0 : y_down; 
		int y_up = ((y_id + num_bricks_height/2) >= grid_height) ? grid_height : (y_id + num_bricks_height/2);
		
		int x_down = (x_id - num_bricks_width/2); x_down = (x_down < 0) ? 0 : x_down; 
		int x_up = ((x_id + num_bricks_width/2) >= grid_width) ? grid_width : (x_id + num_bricks_width/2);
		
		left_down = new Pair<int, int>(x_down,y_down);
		right_up = new Pair<int, int>(x_up,y_up);
	}

	private bool isOut(int si, int sj){
		return(si < 0 || si >= grid_height || sj < 0 || sj >= grid_width);
	}

	public class Wall {
		float counter;
		float time_to_grow;
		short dir;
		public bool done;
		bool odone, tdone;
		int num_bricks_width, num_bricks_height;
		int x_id=0, y_id=0;


		bool glob_odone, glob_tdone;

		int oi,oj,oybound,oxbound; //first direction id
		int ti,tj,tybound,txbound; //second direction id
		int tmp_width, tmp_height;
		metagrid g;
		int ocount, tcount;
		float factor_z, factor_x;

        uint cnt_forward = 0;
        uint cnt_backward = 0;

		Pair<int,int> left_down, right_up;

        private WallSegment wf, ws;
        private GameObject walls; 
        private bool preview;

		public void getRegion(out int xl, out int yl, out int xr, out int yr)
		{
            xl = ws.end_y;
            yl = ws.end_x;
            xr = wf.end_y;
            yr = wf.end_x;
        }

		public short getDirection()
		{
			return dir;
		}

        public Wall(Wall w)
        {
            walls = w.walls;
            preview = w.preview;
            counter = w.counter;
            time_to_grow = w.time_to_grow;
            dir = w.dir;
            done = w.done;
            odone = w.odone;
            tdone = w.tdone;
            num_bricks_width = w.num_bricks_width;
            num_bricks_height = w.num_bricks_height;
            x_id = w.x_id;
            y_id = w.y_id;


            glob_odone = w.glob_odone;
            glob_tdone = w.glob_tdone;

            oi = w.oi;
            oj = w.oj;
            oybound = w.oybound;
            oxbound = w.oxbound; 

            ti = w.ti;
            tj = w.tj;
            tybound = w.tybound;
            txbound = w.txbound;

            tmp_width = w.tmp_width;
            tmp_height = w.tmp_height;
            g = w.g;
            ocount = w.ocount;
            tcount = w.tcount;
            factor_z = w.factor_z;
            factor_x = w.factor_x;

            cnt_forward = w.cnt_forward;
            cnt_backward = w.cnt_backward;

            left_down = w.left_down;
            right_up = w.right_up;

            wf = w.wf;
            ws = w.ws;
        }

		//short dir: 0 left right, 1 up down
		public Wall (metagrid g_, float ttg, Vector2 start, short di, float height, bool is_preview)
        {
            preview = is_preview;
			glob_odone = false;
			glob_tdone = false;
			dir = di; g = g_;
			time_to_grow = ttg;
			done = false; odone = false; tdone = false;
			counter = 0; ocount = 0; tcount = 0;
			g.getCellCoords (start,out x_id,out y_id);

			if(dir == 1){
				num_bricks_width = g.numblocks_updown.Second;
				num_bricks_height = g.numblocks_updown.First;
			} else {
				num_bricks_width = g.numblocks_leftright.Second;
				num_bricks_height = g.numblocks_leftright.First;
			}

			g.BlockRegion(new Pair<int, int>(x_id,y_id),out left_down,out right_up,dir);
			oi = left_down.Second;oj = left_down.First;oybound = right_up.Second; oxbound = right_up.First;
			ti = left_down.Second;tj = left_down.First;tybound = right_up.Second; txbound = right_up.First;

			tmp_width = num_bricks_width;
			tmp_height = num_bricks_height;
					
            //g.grid[y_id, x_id].clicked = true;


            if(dir == 1)
            {
                for (int y = y_id+1; y < g.grid.GetLength(0); y++)
                {
                    if (g.grid[y, x_id].contains_wall)
                        break;
                    cnt_forward++;
                }

                for (int y = y_id - 1; y >= 0; y--)
                {
                    if (g.grid[y, x_id].contains_wall)
                        break;
                    cnt_backward++;
                }
            } else
            {
                for (int x = x_id+1; x < g.grid.GetLength(1); x++)
                {
                    if (g.grid[y_id, x].contains_wall)
                        break;
                    cnt_forward++;
                }

                for (int x = x_id - 1; x >= 0; x--)
                {
                    if (g.grid[y_id, x].contains_wall)
                        break;
                    cnt_backward++;
                }

            }

            walls = new GameObject();
            walls.name = "fence";
            wf = walls.AddComponent<WallSegment>();
            wf.init(g, x_id, y_id, (dir == 1) ? Direction.VERT : Direction.HOR, Direction.FOR, 
                num_bricks_width, num_bricks_height, (int)cnt_forward, height,preview);

            ws = walls.AddComponent<WallSegment>();
            ws.init(g, x_id, y_id, (dir == 1) ? Direction.VERT : Direction.HOR, Direction.BACK, 
                num_bricks_width, num_bricks_height, (int)cnt_backward, height,preview);

            if(preview)
            {
                wf.setPreview();
                ws.setPreview();
            }
        }

        public void disablePreview()
        {
            if (preview)
            {
                wf.disablePreview();
                ws.disablePreview();
            }
        }

        public void enablePreview()
        {
            if (preview)
            {
                wf.enablePreview();
                ws.enablePreview();
            }
        }

        public void destroyPreview()
        {
            if (preview)
            {
                wf.destroyPreview();
                ws.destroyPreview();
                Destroy(walls);
            }
        }

        public void Update(){
            done = odone && tdone;
            if (done) return;
			if(counter >= time_to_grow){
				counter = 0;
                wf.setWall(); odone = wf.advance();
                ws.setWall(); tdone = ws.advance();
			}
			counter += Time.deltaTime;
		}

		public void deactivate_area(Vector3 sheep_pos)
		{
            if (!preview)
            {
                wf.deactivate_area(sheep_pos);
                ws.deactivate_area(sheep_pos);
            }
		}
	}
	
	List<Wall> walls;

	bool pressed_mouse;
	public float mouse_delay;
	float mouse_counter;

    private Wall wiew;
    private Wall wall_preview_north_south;
    private Wall wall_preview_west_east;
	// Use this for initialization
	void Start () {

        wall_preview_west_east = null;
        wall_preview_north_south = null;
        numFilledCells = 0;
		pressed_mouse = false;
		mouse_counter = 0;
		walls = new List<Wall>();
		//right boundary x
		rx = minmaxVec (trverts ("r"),0,Mode.MIN);
		//left boundary x
		lx = minmaxVec (trverts ("l"),0,Mode.MAX);
		//lower boundary z
		dz = minmaxVec (trverts ("d"),2,Mode.MAX);
		//upper boundary z
		uz = minmaxVec (trverts ("u"),2,Mode.MIN);

		width = rx - lx;
		height = uz - dz;
		h_width = width/grid_width;
		h_height = height/grid_height;

		grid = new Cell[grid_height,grid_width];
		for(int i = 0; i < grid_height;++i){
			for(int j = 0; j < grid_width;++j){
				grid[i,j] = new Cell(this,j,i);
			}
		}

		//updown
		{
			int num_bricks_width = (int) (wall_width / h_width);
			int num_bricks_height = (int) (wall_height / h_height);
			num_bricks_height+=num_bricks_height%2;
			num_bricks_width+=num_bricks_width%2;
			num_bricks_height+=1; num_bricks_width+=1;
			numblocks_updown = new Pair<int, int>(num_bricks_height,num_bricks_width);
		}

		//leftright
		{
			int num_bricks_height = (int) (wall_width / h_width);
			int num_bricks_width = (int) (wall_height / h_height);
			num_bricks_height+=num_bricks_height%2;
			num_bricks_width+=num_bricks_width%2;
			num_bricks_height+=1; num_bricks_width+=1;
			numblocks_leftright = new Pair<int, int>(num_bricks_height,num_bricks_width);
		}

        GameObject RuleObject = new GameObject();
        gamerule = RuleObject.AddComponent < RULE.DefaultRule > ();
        gamerule.init(this);
    }

	bool WallisDone(Wall w)
	{
        if (w.done)
        {
            //Debug.Log("setting wall metagrid " + Time.fixedTime);
            gamerule.setWall(new Wall(w));
        }
        return w.done;
	}

	Vector2 last_mouse_pos;


	// Update is called once per frame
	void Update () {

		if (!gamerule.isValid ()) 
		{
			SceneManager.LoadScene("gameover");
		}

		if(gamerule.won()){
            //load next level - TODO
            SceneManager.LoadScene("gameover");
        }

        //   EditorApplication.currentScene
        Scene s = SceneManager.GetActiveScene();
        if (s.name == "start_menue") return;

		if(draw_grid){
			for(int i = 0; i < grid_height;++i){
				for(int j = 0; j < grid_width;++j){
					grid[i,j].draw();
                }
			}

		}

        if (Input.GetMouseButton(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit info;
            if (!pressed_mouse)
            {
                pressed_mouse = true;
                if (GameObject.Find("Terrain").GetComponent<Collider>().Raycast(ray, out info, 1000.0F))
                {
                    Collider col = info.collider;

                    if (col.name == "Terrain" && (info.point.x <= lx || info.point.x >= rx || info.point.z >= uz || info.point.z <= dz))
                    {
                        pressed_mouse = false; //mouse_counter = 0;
                    }

                    else if (col.name == "Terrain")
                    {
                        int x_id = 0, y_id = 0;
                        getCellCoords(new Vector2(info.point.x, info.point.z), out x_id, out y_id);
                        last_mouse_pos = new Vector2(info.point.x, info.point.z);
                    }
                }
            }
            else
            {
                if (GameObject.Find("Terrain").GetComponent<Collider>().Raycast(ray, out info, 1000.0F))
                {
                    int x_id = 0, y_id = 0;
                    getCellCoords(new Vector2(info.point.x, info.point.z), out x_id, out y_id);

                    Vector2 mouse_pos = new Vector2(info.point.x, info.point.z);
                    float ang = Vector2.Angle(last_mouse_pos - mouse_pos, new Vector2(-1.0F, 0));
                    short dir = (short)((ang < 45.0F || ang >= 135.0F) ? 0 : 1);

                    int old_x, old_y;
                    getCellCoords(last_mouse_pos, out old_x, out old_y);

                    if (gamerule.canSetWall(old_y, old_x))
                    {
                        if(dir == 0)
                        {
                            if(wall_preview_north_south == null)
                            {
                                wall_preview_north_south = new Wall(this, time_to_grow, last_mouse_pos, dir, height_of_fence_real_world, true);
                            }
                            wall_preview_north_south.enablePreview();
                            if (wall_preview_west_east != null)  wall_preview_west_east.disablePreview();
                        } else
                        {
                            if (wall_preview_west_east == null)
                            {
                                wall_preview_west_east = new Wall(this, time_to_grow, last_mouse_pos, dir, height_of_fence_real_world, true);
                            }
                            if(wall_preview_north_south != null) wall_preview_north_south.disablePreview();
                            wall_preview_west_east.enablePreview();
                        }

                    }
                }

            }
        }
        else if (pressed_mouse)
        {

            if (wall_preview_north_south != null)
            {
                wall_preview_north_south.destroyPreview();
                wall_preview_north_south = null;
            }
            if (wall_preview_west_east != null)
            {
                wall_preview_west_east.destroyPreview();
                wall_preview_west_east = null;
            }
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit info;

            if (GameObject.Find("Terrain").GetComponent<Collider>().Raycast(ray, out info, 1000.0F))
            {
                int x_id = 0, y_id = 0;
                getCellCoords(new Vector2(info.point.x, info.point.z), out x_id, out y_id);

                Vector2 mouse_pos = new Vector2(info.point.x, info.point.z);
                float ang = Vector2.Angle(last_mouse_pos - mouse_pos, new Vector2(-1.0F, 0));
                short dir = (short)((ang < 45.0F || ang >= 135.0F) ? 0 : 1);
                if (walls.Count == 0 && !isOut(x_id, y_id))
                {
                    int old_x, old_y;
                    getCellCoords(last_mouse_pos, out old_x, out old_y);

                    if (gamerule.canSetWall(old_y, old_x))
                        walls.Add(new Wall(this, time_to_grow, last_mouse_pos, dir, height_of_fence_real_world,false));

                }
            }

            pressed_mouse = false;
        }

        foreach (Wall w in walls)
		{
			w.Update();
		}

        int num = walls.RemoveAll(WallisDone);

        numFilledCells = 0;
   /*     for (int i = 0; i < grid_height; ++i)
        {
            for (int j = 0; j < grid_width; ++j)
            {
                if (grid[i, j].deactivated || grid[i, j].contains_wall) numFilledCells++;
               
            }
        }*/

        levelsel.setCells(numFilledCells,(grid_height*grid_width)-numFilledCells);
	}
}
