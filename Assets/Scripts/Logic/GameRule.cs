using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using WallAssembly;
using Wall=metagrid.Wall;
using Cell=metagrid.Cell;
namespace RULE {

	public abstract class GameRule : MonoBehaviour
    {

		protected metagrid grid;
		protected List<Wall> walls;

		public void init(metagrid g)
		{
			grid = g;
            walls = new List<Wall>();
		}

        public void Update() { }
		
		public abstract bool isValid ();
		public abstract bool canSetWall(int x, int y);
		public abstract void setWall (Wall w);
		public abstract bool won();
	}

	public class DefaultRule : GameRule
	{
		private int numDeactivatedCells;
		private float percentage_win = 0.60f;

		private struct region {
			//lower left
			public int xl, yl;
			//upper right
			public int xr, yr;

            public Color col;

			public region(int _xl, int _yl, int _xr, int _yr,Color c)
			{
				xl = _xl; yl = _yl; xr = _xr; yr = _yr;
                col = c;
			}

			public bool regionContainsSheep(List<simple_sphere> sheeps,metagrid g)
			{
				foreach (simple_sphere sheep in sheeps) 
				{
					int sx, sy;
					sheep.getPos(out sx, out sy);
                    if (sx >= xl && sx <= xr && sy >= yl && sy <= yr)
					{
                        return true;
                    }
				}
				return false;
			}

			public int setDeactivated(metagrid g)
			{
				int numCells = 0;
				for (int x = xl; x <= xr;++x){
					for (int y = yl; y <= yr;++y){

                        Cell c = g.getCell(x, y);
                       /* GameObject brick = (GameObject)Instantiate(g.brick, new Vector3(c.pos.x, 0.5F, c.pos.y), Quaternion.identity);
                        brick.transform.localScale = new Vector3(g.h_width, 5.5F, g.h_height);
                        brick.GetComponent<Renderer>().material.color = col;*/

                        c.deactivated = true;

                        numCells++;
					}
				}
				return numCells;
			}

            public void draw(metagrid g)
            {

                for (int x = xl; x <= xr; ++x)
                {
                    for (int y = yl; y <= yr; ++y)
                    {

                        Cell c = g.getCell(x, y);
                        GameObject brick = (GameObject)Instantiate(g.brick, new Vector3(c.pos.x, 0.5F, c.pos.y), Quaternion.identity);
                        brick.transform.localScale = new Vector3(g.h_width, 5.5F, g.h_height);
                        brick.GetComponent<Renderer>().material.color = col;
                        //c.draw(new Vector3(0,2,0),col);
                        //Debug.Log("drawing");
                    }
                }
            }

            public void print()
            {
                Debug.Log("region = "+xl+"/"+yl+" "+xr+"/"+yr);
            }


		}

		private List<region> contains_sheep;
		private List<region> contains_no_sheep;

		public DefaultRule()
        {
            numDeactivatedCells = 0;
            contains_sheep = new List<region>();
            contains_no_sheep = new List<region>();
        }

		public override bool won()
		{
			float numcells = grid.grid_height * grid.grid_width;
			float deactive = numDeactivatedCells;
			float percentage = deactive / numcells;
            //Debug.Log("percentage = " + percentage);
            return percentage >= percentage_win;
            
		}

		public override bool isValid()
		{
			/*List<Pair<int,int>> positions = new List<Pair<int, int>>();
			foreach (simple_sphere sheep in grid.sheeps) 
			{
				//get cellpositions
				int x, y;
				sheep.getPos(out x,out y);
				positions.Add(new Pair<int,int>(x,y));
			}*/
            //metagrid.Cell cell = grid.getCell(x,y);
            return true;
		}

		public override bool canSetWall(int x, int y)
		{	
			Cell c = grid.getCell (x, y);
			return !(c.contains_wall || c.deactivated);
		}

		public override void setWall(Wall w)
		{
			walls.Add(w);
			region r = new region();
			//region of fence
			w.getRegion (out r.xl, out r.yl, out r.xr, out r.yr);
            r.col = new Color(1.0f, 0, 0);

            //r.draw(grid);

			//divide and conquer
			short dir = w.getDirection ();
			List<region> regions = new List<region>();

			if (dir == 1) {
                //vertical
                //Debug.Log("scanning vertical");
				int xstart=-1, xend=-1;

                //search to left
                int y = r.xr;

                if(r.xr < r.xl)
                {
                    y += System.Math.Abs((r.xr - r.xl)) /2;
                } else y -= System.Math.Abs((r.xr - r.xl)) /2;

                //r.print();
                for (int x = r.yl-1; x >= 0; --x) {
					Cell c = grid.getCell (y, x);
                    xstart = x;
					if (c.contains_wall) {
						xstart = x + 1;
						break;
					}
				}

				//search to right
				for (int x = r.yr+1; x < grid.grid_width; ++x) {
					Cell c = grid.getCell (y, x);
                    xend = x;
                    if (c.contains_wall) {
						xend = x - 1;
						break;
					}
				}

                if (xstart != -1) regions.Add(new region(r.xl,xstart, r.xr, r.yl - 1, new Color(0, 1, 0)));
                if (xend != -1) regions.Add(new region(r.xl, r.yl, r.xr ,xend, new Color(0, 0, 1)));
            } else {
				//horizontal
				int ystart=-1, yend=-1;
				
				//search down
				int x = r.yl;

                if (r.yl < r.yr)
                {
                    x += System.Math.Abs((r.yr - r.yl)) / 2;
                }
                else x -= System.Math.Abs((r.yr - r.yl)) / 2;

               for (int y = r.xl-1; y >= 0; --y) {
					Cell c = grid.getCell (y, x);
                    ystart = y;
                    if (c.contains_wall) {
						ystart = y + 1;
						break;
					}
				}

				//search up
				for (int y = r.xr+1; y < grid.grid_height; ++y) {
					Cell c = grid.getCell (y, x);
                    yend = y;
                    if (c.contains_wall) {
						yend = y - 1;
						break;
					}
				}

                if (ystart != -1) regions.Add(new region(ystart,r.yl, r.xl - 1, r.yr, new Color(0, 0.5f, 0.5f)));
                if (yend != -1)
                    regions.Add(new region(r.xl + 1,r.yl,yend,r.yr, new Color(0.5f, 0.0f, 0.5f)));
            }


            foreach (region reg in regions) {
                if (reg.regionContainsSheep(grid.sheeps,grid))
                {
                    contains_sheep.Add(reg);
                }
                else
                {
                   contains_no_sheep.Add(reg);
                   numDeactivatedCells += reg.setDeactivated (grid);
                }

			}
		}
	}
}
