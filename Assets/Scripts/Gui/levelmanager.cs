using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class levelmanager : MonoBehaviour
	{

		public void changeScene(int i)
		{
			Scene s = SceneManager.GetSceneAt(i);
			SceneManager.LoadScene(s.buildIndex);
		}

		public void changeScene(string s)
		{
			SceneManager.LoadScene(s);
		}

		public void exitGame()
		{
			Application.Quit();
		}
	}
}
