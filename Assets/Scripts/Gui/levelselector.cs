﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class levelselector : MonoBehaviour {

    private int filledCells;
    private int freeCells;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        // Make a background box
        GUI.Box(new Rect(10, 10, 100, 30 + SceneManager.sceneCount*30 + 30 + 30), "Select level");

        float h = 40;
        string name;
        for(int i = 0; i < SceneManager.sceneCount; ++i)
        {
            Scene s = SceneManager.GetSceneAt(i);

            name = s.name;
            if (GUI.Button(new Rect(20, h, 80, 20), name))
            {
                SceneManager.LoadScene(s.buildIndex);
            }

            h += 30;
        }

        name = filledCells.ToString();
        
        GUI.TextField(new Rect(20, h, 80, 20),name);
        h += 30;
        name = freeCells.ToString();
        GUI.TextField(new Rect(20, h, 80, 20), name);
    }

    public void setCells(int filled, int free)
    {
        freeCells = free;
        filledCells = filled;
    }

    public int getFreeCells()
    {
        return freeCells;
    }
    public int getFilledCells()
    {
        return filledCells;
    }
}
