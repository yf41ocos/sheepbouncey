﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WallAssembly;

public class simple_sphere : MonoBehaviour {

    private Vector3 direction;
    private Vector3 orig_dir;

    public metagrid grid;
	public int id;
    public float speed = 10.0F;

    public GameOverManager mg;

    private Animation anim;
    private Rigidbody rigid;

	Vector3 force_dir(Vector2 input) {
		Vector3 res = new Vector3(input.x,0,input.y);
		return res;
	}

	float AngleVector2D(Vector3 from, Vector3 to){
		float dot = from.x * to.x + from.z * to.z;
		float det = from.x * to.z - from.z * to.x;
		double angle = System.Math.Atan2(det,dot);
		return (float)((-180.0F / System.Math.PI) * angle);
	}
  
    // Use this for initialization
    void Start () {

        anim = transform.GetComponentInChildren<Animation>();
        rigid = transform.GetComponent<Rigidbody>();

        foreach (AnimationState state in anim)
        {
            state.speed = speed/3.5F;
        }


        GameObject canvas = GameObject.Find("Canvas");
        mg = canvas.GetComponent<GameOverManager>();
        grid = GameObject.FindObjectOfType<metagrid>();

		grid.enlistSheep(this);

        orig_dir = new Vector3(0,0,1);
		direction = force_dir(Random.insideUnitCircle);	direction.Normalize();
        rigid.rotation = Quaternion.AngleAxis(AngleVector2D(orig_dir, direction), new Vector3(0, 1, 0));
    }


    Bounds getConvexHull()
    {
        var bounds = new Bounds(transform.parent.transform.position, Vector3.zero);
        Vector3[] verts = transform.GetComponent<MeshFilter>().sharedMesh.vertices;
        foreach (Vector3 v in verts)
        {
            Vector3 vWorld = transform.parent.transform.TransformPoint(v);
            bounds.Encapsulate(vWorld);
        }
        Collider c = transform.GetComponent<Collider>();

        return c.bounds;
    }


	void OnCollisionEnter(Collision col){


            Vector3 n = col.contacts[0].normal;
            direction = Vector3.Reflect(direction, n);
            direction.Normalize();
            rigid.rotation = Quaternion.AngleAxis(AngleVector2D(orig_dir, direction), new Vector3(0, 1, 0));
       
          /*  if (col.gameObject.name == "prefab_post(Clone)" ||
                col.gameObject.name == "prefab_plank_double(Clone)")
            {
                GameObject parent = col.transform.parent.gameObject;
                if (parent.tag == "is_building")
                {
                    //Debug.Log("you lost");
                    //mg.gameover = true;
                }

            }*/
    }

    void getCells(out int j, out int i, out int s, out int t){
		Vector2 pos = new Vector2(transform.parent.transform.position.x,transform.parent.transform.position.z);
		float radius = 0.5F;
		grid.getCellCoords(pos - new Vector2(radius,radius),out j,out i);
		grid.getCellCoords(pos + new Vector2(radius,radius),out s,out t);
	}

	//swaps x, y
	public void getPos(out int x, out int y)
	{
		Vector3 rpos = rigid.position;
		grid.getCellCoords(new Vector2(rpos.x, rpos.z), out y, out x);
	}

	// Update is called once per frame - after physics !
	void Update () {

        int sx, sy; 
        Vector3 rpos = rigid.position;
        grid.getCellCoords(new Vector2(rpos.x, rpos.z), out sx, out sy);
        if(grid.getCell(sy, sx).deactivated)
        {
            mg.gameover = true;
        }

        rigid.velocity = direction * speed;
    }
}
