Game prototype written in C# using the Unity engine.

In the game you have to divide the sheep without them touching the fences while they are being built.

![Picture showing the Game](https://abload.de/img/photo_2016-04-12_12-39sqzz.jpg)

Debugging of the game logic

![alt text](https://abload.de/img/photo_2016-04-10_00-029pee.jpg)

![alt text](https://abload.de/img/photo_2016-04-10_00-0l8p44.jpg)
